# Plugins - DAIMON Project (TU Clausthal)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

-   [Badges](#badges)
-   [About the Project](#about-the-project)
-   [Documentation](#documentation)
-   [How to Install](#how-to-install)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Badges

[![pipeline status](https://gitlab.com/tuc_graphql/plugins/badges/master/pipeline.svg)](https://gitlab.com/tuc_graphql/plugins/commits/master)
[![release](https://img.shields.io/badge/release-v0.1.0-brightgreen.svg)](https://gitlab.com/tuc_graphql/plugins/tags/v0.1)

[![node](https://img.shields.io/badge/node-v8.11.4-blue.svg)](https://docs.npmjs.com/)
[![npm](https://img.shields.io/badge/npm-v5.10.0-blue.svg)](https://docs.npmjs.com/)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/tuc_graphql/plugins/blob/master/LICENSE)

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

## About the Project

Plugins for the [core](https://gitlab.com/tuc_graphql/core) backend for DAIMON Project (TU Clausthal).

## Documentation

For the **Documentation** and **How to Use** please visit one of the following websites:

-   core

    -   [hosted wiki with Hugo](https://tuc_graphql.gitlab.io/core)
    -   [project wiki on GitLab](https://gitlab.com/tuc_graphql/core/wikis/home)

-   plugins

    -   [hosted wiki with Hugo](https://tuc_graphql.gitlab.io/plugins)
    -   [project wiki on GitLab](https://gitlab.com/tuc_graphql/plugins/wikis/home)

## How to Install

### Docker Installation

-   Clone repository:

```console
git clone https://gitlab.com/tuc_graphql/plugins.git
```

-   Switch branch:

```sh
# Replace {BRANCH} with master (production) or staging (developement)
git checkout {BRANCH}
```

-   Login to [GitLab container registry](https://gitlab.com/help/user/project/container_registry):

```console
docker login registry.gitlab.com
```

-   Compose plugins with core:

```console
docker-compose up
```
